package eu.schepsen.codeforces.ui.handlers;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdapterManager;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import eu.schepsen.codeforces.Activator;

public class DTCHandler extends AbstractHandler
{

    public DTCHandler()
    {
    }

    @Override
    public Object execute(ExecutionEvent event) throws ExecutionException
    {
        Job job = new Job(Activator.PLUGIN_NAME)
        {
            @Override
            protected IStatus run(IProgressMonitor monitor)
            {
                Display.getDefault().asyncExec(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        IWorkbenchWindow window = null;

                        try
                        {
                            window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
                        }
                        catch (ExecutionException e)
                        {
                            e.printStackTrace();
                        }

                        Object sel = ((IStructuredSelection) window.getSelectionService().getSelection()).getFirstElement();

                        IAdapterManager manager = Platform.getAdapterManager();

                        IFile file = (IFile) manager.getAdapter(sel, IFile.class);

                        /** Contest Identification Number **/
                        String contest = file.getParent().getName();
                        /** <b>Name</b> of the Task **/
                        String task = file.getName().substring(0, file.getName().lastIndexOf("."));
                        /** <b>Source Code</b> of the Task's Web Page **/
                        Document doc;
                        /**
                         * 
                         * <b>TC</b> for the current selected Task
                         **/                      
                        Elements tCs;
                        /** <b>TC</b> input **/
                        IFile input;
                        /** <b>TC</b> output **/
                        IFile output;
                        /** InputStream **/
                        InputStream source;          
                        
                        if (!contest.matches("[0-9]{3}"))
                        {
                            MessageDialog.openInformation(
                                    window.getShell(), 
                                    Activator.PLUGIN_NAME,
                                    "The folder containing this file does not match the pattern. Please rename it!"
                                    );

                            return;
                        }

                        if (!task.matches("[A-F]{1}"))
                        {
                            MessageDialog.openInformation(
                                    window.getShell(), 
                                    Activator.PLUGIN_NAME,
                                    "The selected file does not match the pattern. Please rename it!"
                                    );
                            return;
                        }

                        try
                        {
                            doc = Jsoup.connect("http://codeforces.com/contest/" + contest + "/problem/" + task).get();

                            tCs = doc.getElementsByTag("pre");

                            for (int i = 0; i < tCs.size(); i += 2)
                            {
                                input = ((IFolder) file.getParent()).getFile(String.format("%s.%d.in", task, (i >> 1) + 1));

                                if (!input.exists())
                                {
                                    byte[] stream = tCs.get(i).html().replace("<br>", "\n").getBytes();
                                    source = new ByteArrayInputStream(stream);
                                    
                                    input.create(source, IResource.NONE, null);
                                }

                                output = ((IFolder) file.getParent()).getFile(String.format("%s.%d.out", task, (i >> 1) + 1));

                                if (!output.exists())
                                {
                                    byte[] stream = tCs.get(i + 1).html().replace("<br>", "\n").getBytes();
                                    source = new ByteArrayInputStream(stream);
                                    
                                    output.create(source, IResource.NONE, null);
                                }
                            }
                        }
                        catch (IOException | CoreException e)
                        {
                            MessageDialog.openInformation(window.getShell(), Activator.PLUGIN_NAME, e.getMessage());
                            
                            return;
                        }                        
                        
                        MessageDialog.openInformation(window.getShell(), Activator.PLUGIN_NAME, "All TCs were downloaded successfully!");
                    }
                });

                return Status.OK_STATUS;
            }
        };
        
        job.schedule();
        return null;
    }
}
