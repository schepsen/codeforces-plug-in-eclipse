## PROJECT ##

* ID: **C**odeForces **P**lug-in for **E**clipse
* Contact: info@schepsen.eu

## USAGE ##

To use this plug-in you have to go to add the update site

* http://schepsen.eu/eclipse/updates

## HOWTO ##

coming soon

## CHANGELOG ##

### CodeForces Plug-in for Eclipse, updated @ 2016-10-20 ###

* Updated Jsoap to the latest version (1.9.2)

### CodeForces Plug-in for Eclipse, updated @ 2015-10-13 ###

* Fixed some bugs
* Improved the UI

### CodeForces Plug-in for Eclipse, updated @ 2015-10-09 ###

* Initial commit